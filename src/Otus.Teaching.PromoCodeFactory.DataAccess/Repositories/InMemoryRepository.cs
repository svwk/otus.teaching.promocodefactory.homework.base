﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<List<T>> FindAllAsync(Func<T, bool> conditionPredicate)
        {
            return Task.FromResult(Data.Where(conditionPredicate).ToList<T>());

        }
        

        public Task<IEnumerable<T>> CreateAsync(T newData)
        {
            if (Data != null)
            {
                if (Data is List<T> list)
                {
                    list.Add(newData);
                }
                else if (Data is Collection<T> collection)
                {
                    collection.Add(newData);
                }
            }
            return Task.FromResult(Data);
        }


        public Task<IEnumerable<T>> UpdateAsync(T updateData)
        {
            if (Data != null)
            {
                var item = Data.FirstOrDefault(x => x.Id == updateData.Id);
                
                if (item == null)
                    throw new Exception("Не найден удаляемый элемент");
                
                if (Data is List<T> list)
                {
                    int i = list.IndexOf(item);
                    list[i] = updateData;
                }
                else if (Data is Collection<T> collection)
                {
                    int i = collection.IndexOf(item);
                    collection[i] = updateData;
                }

            }
            return Task.FromResult(Data);
        }

        public Task<IEnumerable<T>> DeleteAsync(T deleteData)
        {
            if (Data != null)
            {
                var item = Data.FirstOrDefault(x => x.Id == deleteData.Id);
                if (item == null)
                    throw new Exception("Не найден удаляемый элемент");
                if (Data is List<T> list)
                {
                    list.Remove(item);
                }
                else if (Data is Collection<T> collection)
                {
                    collection.Remove(item);
                }

            }
            return Task.FromResult(Data);
        }

    }

}