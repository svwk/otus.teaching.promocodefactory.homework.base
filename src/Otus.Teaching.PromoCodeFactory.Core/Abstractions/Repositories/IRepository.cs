﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T>
        where T: BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();
        
        Task<T> GetByIdAsync(Guid id);

        Task<List<T>> FindAllAsync(Func<T,bool> conditionPredicate);

        Task<IEnumerable<T>> CreateAsync(T newData);

        Task<IEnumerable<T>> UpdateAsync(T updateData);

        Task<IEnumerable<T>> DeleteAsync(T deleteData);

    }
}