﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.mappers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v2/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly IEmployeeMapper _employeeMapper;

        public EmployeesController(IRepository<Employee> employeeRepository, IRepository<Role> roleRepository)
        {
            _employeeRepository = employeeRepository;
            _roleRepository = roleRepository;
            _employeeMapper = new EmployeeMapper(_employeeRepository,_roleRepository);
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }

        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return new ObjectResult(employeeModel);
        }

        /// <summary>
        /// Добавить нового сотрудника
        /// </summary>
        /// <param name="employerModel">Новые данные</param>
        /// <returns>Информация о добавленном сотруднике</returns>
        [HttpPost]
        public async Task<ActionResult> CreateEmployeeAsync(EmployerEditResponse employerModel)
        {
            if (employerModel == null)
                return BadRequest();

            Employee newEmployee= await _employeeMapper.MapFromModelAsync(employerModel);
 
            try
            {
                await _employeeRepository.CreateAsync(newEmployee);

            }
            catch (Exception e)
            {
                return BadRequest($"Невозможно добавить данные. Возникла следующая ошибка {e.Message}");
            }

            return Ok(_employeeMapper.MapToModelAsync(newEmployee));
        }

        /// <summary>
        /// Редактировать сотрудника
        /// </summary>
        /// <param name="id">id сотрудника</param>
        /// <param name="employerModel">Новые данные</param>
        /// <returns>Отредактированная информация о сотруднике</returns>
        [HttpPut]
        public async Task<ActionResult> EditEmployeeAsync(Guid id,EmployerEditResponse employerModel)
        {
            if (employerModel == null)
                return BadRequest();

            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            await _employeeMapper.MapFromModelAsync(employerModel,employee);

            try
            {
                await _employeeRepository.UpdateAsync(employee);
            }
            catch (Exception e)
            {
                return BadRequest($"Невозможно изменить данные. Возникла следующая ошибка {e.Message}");
            }

            return Ok(_employeeMapper.MapToModelAsync(employee));
        }


        /// <summary>
        /// Удалить сотрудника
        /// </summary>
        /// <param name="id">id сотрудника</param>
        /// <returns>Информация об удаленном сотруднике</returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> DeketeEmployeeAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            try
            {
                await _employeeRepository.DeleteAsync(employee);
            }
            catch (Exception e)
            {
                return BadRequest($"Невозможно удалить данные. Возникла следующая ошибка {e.Message}");
            }

            return Ok(_employeeMapper.MapToModelAsync(employee));
        }


    }
}