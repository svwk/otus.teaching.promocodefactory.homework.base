﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.mappers
{
    public interface IEmployeeMapper
    {
        public Task<Employee> MapFromModelAsync(EmployerEditResponse employerModel, Employee newEmployee = null);
        public EmployerEditResponse MapToModelAsync(Employee employee, EmployerEditResponse employerModel = null);
    }

}
