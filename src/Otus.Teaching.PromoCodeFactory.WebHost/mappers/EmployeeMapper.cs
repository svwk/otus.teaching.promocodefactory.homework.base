﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;


namespace Otus.Teaching.PromoCodeFactory.WebHost.mappers
{
    class EmployeeMapper : IEmployeeMapper
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _roleRepository;

        public EmployeeMapper(IRepository<Employee> employeeRepository, IRepository<Role> roleRepository)
        {
            _employeeRepository = employeeRepository;
            _roleRepository = roleRepository;
        }


        public async Task<Employee> MapFromModelAsync(EmployerEditResponse employerModel, Employee newEmployee=null)
        {
            if (newEmployee == null)
            {
                newEmployee = new Employee {Id = Guid.NewGuid()};
            }
            var employeeRoles = await _roleRepository.FindAllAsync(x => employerModel.Roles.Contains(x.Name));

            newEmployee.FirstName = employerModel.FirstName;
            newEmployee.LastName = employerModel.LastName;
            newEmployee.Email = employerModel.Email;
            newEmployee.Roles = employeeRoles;
            newEmployee.AppliedPromocodesCount = employerModel.AppliedPromocodesCount;

            return newEmployee;
        }


        public EmployerEditResponse MapToModelAsync(Employee employee, EmployerEditResponse employerModel = null)
        {
            if (employerModel == null)
            {
                employerModel = new EmployerEditResponse( );
            }

            List<string> employeeRoles = employee.Roles.Select(x => x.Name).ToList();

            employerModel.FirstName = employee.FirstName;
            employerModel.LastName = employee.LastName;
            employerModel.Email = employee.Email;
            employerModel.Roles = employeeRoles;
            employerModel.AppliedPromocodesCount = employee.AppliedPromocodesCount;

            return employerModel;
        }


    }
}